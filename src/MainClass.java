import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class MainClass {

	public static ArrayList<Integer> arr = new ArrayList<>();
//	static Iterator<Integer> iter = arr.iterator();
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Commands:\n 1 = Add\n 2 = Remove\n 3 = Size\n 4 = Print List\n 0 = End\n");
		
		
		askForInput();
	}

	private static void askForInput(){
		System.out.println("Please introduce a command:");
		
		String input = sc.nextLine();
		checkInput(input);
		
	}
	
	private static void checkInput(String input){
		
		if(input.length() == 0){
			System.out.println("Invalid Command");
			askForInput();
		}else{
			
			if(input.length() == 1){
			
				int command = Integer.parseInt(Character.toString(input.charAt(0)));
				if(command == 3 || command == 4 || command == 0){
				proccesInput(0,command);
				}else{
				System.out.println("Invalid Command");
				askForInput();
				}
			}else{
			
			int command = Integer.parseInt(Character.toString(input.charAt(0)));
			if(input.charAt(1) != ' '){
				int number = Integer.parseInt(input.substring(1, input.length()));
				proccesInput(number,command);
			}else if(input.charAt(1) == ' '){
				int number = Integer.parseInt(input.substring(2, input.length()));
				proccesInput(number,command);
			}
		  }
		}
		
	}
	
	private static void proccesInput(int number, int command){
		
		if(command == 1){
			arr.add(number);
			System.out.println("Added: "+number +" to the list");
			System.out.println("");
		}else if(command == 2){
		//remove all number X in list
		removeNumber(number);
			
		}else if(command == 3){
			//Print List
			System.out.println(arr.size());
			System.out.println("");
		}else if(command == 4){
			//Print List
			printList();
		}else if(command == 0){
			sc.close();
			System.exit(0);
		}
		
		askForInput();
		
	}
	
	private static void removeNumber(int number){
		//int count = 0;
		Iterator<Integer> iter = arr.iterator();
		while(iter.hasNext()){
			if(iter.next() == number){
			iter.remove();
			}
//			else{
//				count ++;
//			}
		}
	}
	
	private static void printList(){
		String s = "[";
	 
		for(int element: arr){
			s = s + (element +",");
		}
		if(!arr.isEmpty()){
		s = s.substring(0, s.length()-1);
		}
		s = s + "]";
		
		System.out.println(s);
		System.out.println("");
		
	}
	
}
